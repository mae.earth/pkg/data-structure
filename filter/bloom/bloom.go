package bloom

import (
	"fmt"
	"sync"
	"crypto/sha1"
)


const (
	DefaultFilterSize uint = 64
)

type filter struct {
	sync.RWMutex

	bits []int
	keys uint
	size uint

	hash Hasher
}

func (f filter) Name() string {
	return "BloomFilter"
}

func (f filter) Size() uint {
	f.RLock()
	defer f.RUnlock()
	return f.size
}

func (f filter) Keys() uint {
	f.RLock()
	defer f.RUnlock()
	return f.keys
}

func (f filter) Stats() *Stats {
	return nil
}

func (f filter) Attribute(domain,token string) interface{} {
	return nil
}

func (f filter) Attributes(domains ...string) map[string]interface{} {
	return nil
}

/* Append */
func (f filter) Append(keys ...string) BloomFilter {
	f.RLock()
	defer f.RUnlock()

	bits := make([]int, f.size)
	if f.keys > 0 {
		copy(bits,f.bits)
	}

	hash := f.hash.Hash

	/* TODO dedup the keys */

	for _,key := range keys {
		m := hash(key, int(f.size))
		bits[ m[0] ] ++
		bits[ m[1] ] ++
		bits[ m[2] ] ++
	}

	return  filter{keys: uint(len(keys)), bits: bits, size: f.size, hash: f.hash }
}

func (f filter) Remove(keys ...string) BloomFilter {
	f.RLock()
	defer f.RUnlock()

	if len(keys) == 0 || f.keys == 0 {
		return f
	}

	bits := make([]int,int(f.size))
	copy(bits,f.bits)

	hash := f.hash.Hash

	for _,key := range keys {
		m := hash(key,int(f.size))
		bits[m[0]] --
		bits[m[1]] --
		bits[m[2]] --
	}

	return filter{keys: f.keys - uint(len(keys)), bits: bits, size: f.size, hash: f.hash }
}


func (f filter) Present(keys ...string) []bool {
	f.RLock()
	defer f.RUnlock()

	if len(keys) == 0  {
		return nil
	}

	out := make([]bool,len(keys))

	if f.keys == 0 {
		return out
	}


	check := func(key string) bool {
		m := f.hash.Hash(key,int(f.size))
		return f.bits[ m[0] ] > 0 && f.bits[ m[1] ] > 0 && f.bits[ m[2] ] > 0
	}

	for i,key := range keys {
		out[i] = check(key)
	}

	return out
}

func (f filter) Equal(other BloomFilter) bool {
	f.RLock()
	defer f.RUnlock()

	if other == nil {
		return false
	}

	if f.size != other.Size() || f.keys != other.Keys() {
		return false
	}

	return true
}

func (f filter) Hash() []byte {
	f.RLock()
	defer f.RUnlock()

	h := sha1.New()
	fmt.Fprintf(h,"size:%d\nkeys:%d\n", f.size, f.keys)
	fmt.Fprintf(h,"%+v\n", f.bits)

	return h.Sum(nil)
}













/* New */
func New() BloomFilter {
	config := new(Configuration)
	config.Filter.Size = DefaultFilterSize
	config.Hash.Hasher = new(DefaultHasher)

	return NewWithConfiguration(config)
}



/* NewWithConfiguration */
func NewWithConfiguration(config *Configuration) BloomFilter {
	if config == nil {
		return New()
	}

	f := filter{keys: 0, bits: nil, size: config.Filter.Size, hash: config.Hash.Hasher }

	return f
}
