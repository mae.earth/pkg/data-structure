package bloom

/* TODO:
 * [ ] Dot(er)
 * [ ] move Attributes interface outside
 * [ ] add versioning for vector clock interface
 */


/* Stats */
type Stats struct {
	Size int
	Bytes int
	Count int
	Ratio float64
	Sparse float64
}

/* Diff */
func (stats *Stats) Diff(o *Stats) *Stats {
	if o == nil {
		return &Stats{Size:0,Bytes:0,Count:0,Ratio:0,Sparse:0}
	}
	return &Stats{Size:stats.Size - o.Size,
								Bytes:stats.Bytes - o.Bytes,
								Count:stats.Count - o.Count,
								Ratio:stats.Ratio - o.Ratio,
								Sparse:stats.Sparse - o.Sparse}
}

/* Configuration */
type Configuration struct {
	Filter struct {
		Size uint
	}
	Hash struct {
		Hasher Hasher
	}
}

/* BloomFilter */
type BloomFilter interface {

	Name() string
	Size() uint
	Keys() uint
	Stats() *Stats
	Attribute(domain,token string) interface{}
	Attributes(domains ...string) map[string]interface{}

	Append(keys ...string) BloomFilter
	Remove(keys ...string) BloomFilter
	Present(keys ...string) []bool

	Equal(other BloomFilter) bool
	Hash() []byte
}

/* Hasher */
type Hasher interface {

	Name() string
	Hash(key string, k int) []int

}


