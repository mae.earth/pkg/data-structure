package bloom

import (
	"fmt"
	"testing"
	. "github.com/smartystreets/goconvey/convey"
)

func ShouldEqualK3(actual interface{}, expected ...interface{}) string {
	if len(expected) != 3 {
		return "expected 3 integers"
	}

	ak,ok := actual.([]int)
	if !ok {
		return "actual should be []int of 3"
	}
	if len(ak) != 3 {
		return fmt.Sprintf("expected actual to be 3 integers but was %d", len(ak))
	}

	k := []int{-1,-1,-1}

	if d,ok := expected[0].(int); ok {
		k[0] = d
	}
	if d,ok := expected[1].(int); ok {
		k[1] = d
	}
	if d,ok := expected[2].(int); ok {
		k[2] = d
	}

	if k[0] != ak[0] || k[1] != ak[1] || k[2] != ak[2]  {
		return fmt.Sprintf("expecting %d %d %d but was %d %d %d", k[0], k[1], k[2], ak[0], ak[1], ak[2])
	}

	return ""
}


func Test_Hash(t *testing.T) {
	Convey("ShouldEqualK3",t,func() {
		So([]int{1,1,1},ShouldEqualK3,1,1,1)
	})

	Convey("data-structure/filter/bloom/hash",t,func() {
		Convey("DefaultFilterSize",func() {
			Convey("foo should translate to 54, 14, 13",func() {
				So(hash("foo",int(DefaultFilterSize)),ShouldEqualK3,54,14,13)
			})
			Convey("Alice was wondering... should translate to 30,21,58",func() {
				So(hash("Alice was wondering...",int(DefaultFilterSize)),ShouldEqualK3,30,21,58)
			})
		})
		Convey("DefaultFilterSize * DefaultFilterSize",func() {
			Convey("foo should translate to 1014,3086,2125",func() {
				So(hash("foo",int(DefaultFilterSize * DefaultFilterSize)),ShouldEqualK3,1014,3086,2125)
			})
			Convey("Alice was wondering... should translate to 606,1621,1850",func() {
				So(hash("Alice was wondering...",int(DefaultFilterSize * DefaultFilterSize)),ShouldEqualK3,606,1621,1850)
			})
		})
	})
}

func Benchmark_HashFooDefaultFilterSize(b *testing.B) {
	for i := 0; i < b.N; i++ {
		hash("foo",int(DefaultFilterSize))
	}
}

func Benchmark_HashAliceWasWonderingDefaultFilterSize(b *testing.B) {
	for i := 0; i < b.N; i++ {
		hash("Alice was wondering...",int(DefaultFilterSize))
	}
}

func Benchmark_HashFooDefaultFilterSize2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		hash("foo",int(DefaultFilterSize * DefaultFilterSize))
	}
}

func Benchmark_HashAliceWasWonderingDefaultFilterSize2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		hash("Alice was wondering...",int(DefaultFilterSize * DefaultFilterSize))
	}
}

