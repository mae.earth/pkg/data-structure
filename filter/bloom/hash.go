package bloom

import (
	"io"
	"github.com/reusee/mmh3"
	"hash/fnv"
)

const (
	DoubleHashI int = 123
)

func hash(key string,k int) []int {

	m := []int{k,k,k}

	h0 := fnv.New64()
	io.WriteString(h0,key)
	hash0 := h0.Sum(nil)

	h1 := mmh3.New128()
	io.WriteString(h1,key)
	hash1 := h1.Sum(nil)

	m[0] += int(hash0[0])
	m[0] += int(hash0[1])
	m[0] += int(hash0[2])
	m[0] += int(hash0[3])
	m[0] += int(hash0[4])
	m[0] += int(hash0[5])
	m[0] += int(hash0[6])
	m[0] += int(hash0[7])

	m[2] += int(hash1[0])
	m[2] += int(hash1[1])
	m[2] += int(hash1[2])
	m[2] += int(hash1[3])
	m[2] += int(hash1[4])
	m[2] += int(hash1[5])
	m[2] += int(hash1[6])
	m[2] += int(hash1[7])
	m[2] += int(hash1[8])
	m[2] += int(hash1[9])
	m[2] += int(hash1[10])
	m[2] += int(hash1[11])
	m[2] += int(hash1[12])
	m[2] += int(hash1[13])
	m[2] += int(hash1[14])
	m[2] += int(hash1[15])

	m[1] = m[0] + (DoubleHashI * m[2]) + (DoubleHashI * DoubleHashI)

	m[0] = m[0] % k
	m[1] = m[1] % k
	m[2] = m[2] % k
	return m
}

/* DefaultHasher */
type DefaultHasher int

/* Name */
func (h *DefaultHasher) Name() string {
	return "murmur3+fnv"
}

/* Hash */
func (h *DefaultHasher) Hash(key string, k int) []int {
	return hash(key,k)
}



