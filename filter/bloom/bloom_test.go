package bloom

import (
	"fmt"
	"encoding/hex"
	"testing"
	. "github.com/smartystreets/goconvey/convey"
)


func ShouldEqualSha1(actual interface{}, expected ...interface{}) string {
	if len(expected) != 1 {
		return "expected hex encoding of sha1 for expected"
	}
	b,ok := actual.([]byte)
	if !ok {
		return "expected []byte for actual, but was not"
	}

	hash,ok := expected[0].(string)
	if !ok {
		return "expecting an sha1 hex encoded string, but was not"
	}

	if hex.EncodeToString(b) != hash {
		return fmt.Sprintf("expected %q but was %q", hash, hex.EncodeToString(b))
	}
	return ""
}




func Test_Bloom(t *testing.T) {
	Convey("data-structure/filter/bloom",t,func() {


		filter := New()
		So(filter,ShouldNotBeNil)
		So(filter.Hash(),ShouldEqualSha1,"1c07c9f4804e7922e878d7c0abed4ee86d606f72")

		Convey("Append",func() {

			f1 := filter.Append("foo","bar","cow")
			So(f1,ShouldNotBeNil)
			So(f1.Equal(filter),ShouldBeFalse)
			So(f1.Hash(),ShouldEqualSha1,"a5850c2bbfa3b81974e00b4f7669cae2fedf1c73")

			Convey("Present",func() {
				Convey("empty",func() {
					checks := filter.Present("foo","bar","cow")
					So(checks,ShouldNotBeNil)
					So(checks,ShouldNotBeEmpty)
					So(len(checks),ShouldEqual,3)
					So(checks[0],ShouldBeFalse)
					So(checks[1],ShouldBeFalse)
					So(checks[2],ShouldBeFalse)
				})
				Convey("full",func() {
					checks := f1.Present("foo","bar","cow")
					So(checks,ShouldNotBeNil)
					So(checks,ShouldNotBeEmpty)
					So(len(checks),ShouldEqual,3)
					So(checks[0],ShouldBeTrue)
					So(checks[1],ShouldBeTrue)
					So(checks[2],ShouldBeTrue)
				})

				Convey("Remove",func() {

					f2 := f1.Remove("foo","bar","cow")
					So(f2.Equal(f1),ShouldBeFalse)
					So(f2.Equal(filter),ShouldBeTrue)
					checks := f2.Present("foo","bar","cow")
					So(checks,ShouldNotBeNil)
					So(len(checks),ShouldEqual,3)
					So(checks[0],ShouldBeFalse)
					So(checks[1],ShouldBeFalse)
					So(checks[2],ShouldBeFalse)
				})



			})
		})
	})
}